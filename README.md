# MrCrudRails

Welcome to your new gem! In this directory, you'll find the files you need to be able to package up your Ruby library into a gem. Put your Ruby code in the file `lib/mr_crud_rails`. To experiment with that code, run `bin/console` for an interactive prompt.

TODO: Delete this and the text above, and describe your gem

## Installation

Add this line to your application's Gemfile:

```ruby
gem "mr_crud_rails", git: "git@bitbucket.org:abhimanyu/mr_crud_rails.git"
```

## use ful links

http://lizabinante.com/blog/creating-a-configurable-ruby-gem/

- to include local gem from path inside gemspec
  https://stackoverflow.com/questions/11427922/how-to-specify-path-for-dependent-gem-in-gemspec-file

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install mr_crud_rails

## Usage

TODO: Write usage instructions here

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake rspec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/mr_crud_rails.

# Firebase linking

1. Option 1
   Setup a node service

- app name
- path
- method
- params
- fetch a custom token for security reasons

# setting up redis and sidekiq in produciton

https://medium.com/@thomasroest/properly-setting-up-redis-and-sidekiq-in-production-on-ubuntu-16-04-f2c4897944b5

# setting up with kubernetes

https://medium.com/herolens-blog/making-a-monolithic-rails-app-scalable-using-kubernetes-b0f27769d8c5

# running sidekiq in docker

https://dev.to/jamby1100/more-than-hello-world-in-docker-run-rails-sidekiq-web-apps-in-docker-1b37
