require "mr_crud_rails/version"
require "mr_crud_rails/configuration"
require "firebase"

module MrCrudRails
  class << self
    attr_accessor :configuration
    attr_accessor :firebase_client
  end

  def self.configuration
    @configuration ||= Configuration.new

  end
  def self.firebase_client
     @firebase_client ||= Firebase::Client.new(@configuration.firebase_config[:firebase_db_url], @configuration.firebase_config[:firebase_private_key_string])
  end
  

  def self.reset
    @configuration = Configuration.new
  end

  def self.configure
    yield(configuration)
  end
  
  class Error < StandardError; end
  # Your code goes here...
    # frozen_string_literal: true
  module Filterable
    extend ActiveSupport::Concern
    included do
      scope :unarchived, -> (query){ where(archived: !query) }
      scope :include_archived, -> { where(nil) }
    end
    module ClassMethods
      cattr_accessor :filters

      # def available_filters(*filters)
      def available_filters(*scopes)
        self.filters ||= []
        self.filters += scopes
        self.filters.uniq!
      end

      def build_search_query(query = 'query', join_by = 'OR', attrs)
        search_q_arr = []
        attrs.each do |attr|
          # search_q_arr << "LCASE(#{attr}) LIKE :#{query}"
          search_q_arr << "LOWER(#{attr}) LIKE :#{query}"
          # search_q_arr << "#{attr} LIKE :#{query}"
        end
        search_q_str = search_q_arr.join(" #{join_by} ")
        "(#{search_q_str})"
      end

      def filter(filtering_params, _current_user = nil)
        results = where(nil)
        # byebug
        # filtering_params = filtering_params.symbolize_keys.slice(*self.filters)

        filtering_params = filtering_params.slice(*self.filters)
        filtering_params.each do |key, value|
          results = results.public_send(key, value) if value.present?
        end
        results
      end
    end
  end

  module ActivityLogger
    class << self
      def activity_logger(options = {})
        if MrCrudRails.firebase_client
          response = MrCrudRails.firebase_client.push(options[:path], options[:data])
          puts "response - #{response}"
        end
        # puts "options - #{options}"
        # fetch firebasetoken
        
        
      end
      

    end
  end


  module MrCrudController
    extend ActiveSupport::Concern
    # before_action :set_resource, except: [:index, :create]
    # before_action :authenticate_user!

    # prepend_before_action :authenticate_jwt!
    included do
      before_action :set_resource, only: [:update, :edit, :show, :destroy]
    end
    # rescue_from ActiveRecord::RecordNotFound, with: :not_found
    def index(options = {})
      # resources = base_index_query.where(query_params)
      #             .order(order_args)
      #             .page(page_params[:page])
      #             .per(page_params[:page_size])
      # puts "current_user - #{current_user}"
      resources = base_index_query.filter(filter_params)
                                  .order(order_args)
                                  .page(page_params[:page])
                                  .per(page_params[:page_size])
      instance_variable_set(plural_resource_variable, resources)
      # log_activity(resources[0], current_user, {
      #   action_name: "index"
      # })
      
      
      
      
      
      render_multiple(options)
    end

    def create(options = {})
      # puts "current_user - #{current_user}"
      set_resource(resource_class.new(resource_params))
      if get_resource.save!
        # render json: get_resource, status: :created
        render json: { "#{resource_name}": get_resource }, status: :created
      else
        render json: get_resource.errors, status: :unprocessable_entity
      end
    end

    def show
      # render json: get_resource
      render json: { "#{resource_name}": get_resource }
    end

    def update(options = {})
      if get_resource.update!(resource_params)
        # ActivityLogger.activity_logger({: "Hello Activity Logger"})
        render json: { "#{resource_name}": get_resource }
      else
        render json: get_resource.errors, status: :unprocessable_entity
      end
    end

    def destroy(options = {})
      # authorize get_resource, :destroy?
      set_resource()
      if(get_resource.has_attribute? :archived)
        # get_resource.update!({archived: resource_params[:archived]})
        get_resource.update!({archived: true})
        # render json: get_resource
        render json: { "#{resource_name}": get_resource }
      else
        get_resource.destroy
        head :no_content
      end
    end

    def really_destroy(options = {})
      # authorize get_resource, :destroy?
      get_resource.destroy
      head :no_content
    end

    private

    def set_resource(resource = nil)
      resource ||= resource_class.find(params[:id])
      instance_variable_set("@#{resource_name}", resource)
    end

    def get_resource
      resource_obj = instance_variable_get("@#{resource_name}")
      # if self.action_name.presence_in(["create", "update", "destroy"])
      # if self.action_name.presence_in(["update", "destroy"])
      #   authorize resource_obj, self.action_name.to_sym?
      # end
      resource_obj
    end

    def resource_params
      @resource_params ||= send("#{resource_name}_params")
    end

    def model_attributes
      # resource_class.attribute_names.map{|s| s.to_sym} — [:created_at, :updated_at]
      resource_class.attribute_names.map(&:to_sym) # — [:created_at, :updated_at, :id]
    end

    def filter_params child_filter_params = {}
      return_filter_params = params.permit(:search)
      return_filter_params = return_filter_params.merge(child_filter_params)
      if(params[:include_archived])
        return_filter_params = return_filter_params.merge(include_archived: true)
      elsif (params[:only_archived])
        return_filter_params = return_filter_params.merge(only_archived: true)
      else
        return_filter_params = return_filter_params.merge(unarchived: true)
      end
      return_filter_params
      # resource_class.attribute_names.map{|s| s.to_sym}
    end

    def not_found
      render json: { message: 'Not found' }, status: :not_found
    end

    def object_params
      params.require(:object).permit(:id, :title)
    end

    def resource_name
      @resource_name ||= controller_name.singularize
    end

    def resource_class
      @resource_class ||= resource_name.classify.constantize
    end

    def base_index_query
      resource_class
    end

    def resource_serializer_name
      "#{resource_name.classify}Serializer"
    end

    def resource_serializer_class
      resource_serializer_name.constantize
    rescue NameError
      false
    end

    def render_multiple(options = {})
      resources = instance_variable_get(plural_resource_variable)
      # action_to_send = "as_json"
      # if resources.respond_to? :mr_as_json
      #   action_to_send = "mr_as_json"
      # end
      # final_resources = []
      # if resource_serializer_class and false
      #   final_resources = resource_serializer_class.new(resources.to_a, {is_collection: true, params: {current_user: current_user}})
      # else
      #   final_resources = resources.as_json
      # end
      final_resources = resources.as_json
      # byebug
      # puts "action_to_send - #{action_to_send}"
      limit_value = resources.limit_value
      total_pages = resources.total_pages
      # total_count = limit_value.to_f * total_pages.to_f
      total_count = resources.total_count
      render json: {
        page: resources.current_page,
        total_pages: total_pages,
        per_page: limit_value,
        page_size: resources.size,
        # total_count: resources.total_count,
        total_count: total_count,
        # "#{resource_name.pluralize}" => resources.send(action_to_send)
        resource_name.pluralize.to_s => final_resources
      }.merge(options)
    end

    # Returns the allowed parameters for searching
    # Override this method in each controller
    # to permit additional parameters to query on
    def query_params
      {}
    end

    # Returns the allowed parameters for pagination
    def page_params
      params.permit(:page, :page_size)
    end

    def order_args
      :created_at
    end

    def plural_resource_variable
      "@#{resource_name.pluralize}"
    end
    private
    def log_activity(resource, user, options = {})
      current_time = Time.now
      action_name = options[:action_name]
      action_type = options[:action_type] || "single" #single / bulk
      action_on_type = resource.class.to_s
      action_on_id = resource.id
      # current_user = user
      action_by = 
      final_options = {
        action_name: action_name,
        action_type: action_type,
        action_on_id: action_on_id,
        action_on_type: action_on_type,
        action_by: user.id,
        created_at: current_time,
        updated_at: current_time,
        message: "#{user.first_name} #{action_name} #{action_on_type}",
        meta: {
          # ip: 
        }

      }
      ActivityLogger.activity_logger({
        action: "push",
        path: "users/#{user.id}",
        data: final_options
      })
      # post/put to firebase at appropriate path
      # require token from node app
      
      
    end
    
  end

end
